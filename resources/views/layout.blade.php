<!DOCTYPE html>
<html lang="en">
<head>
    <title>{{env('APP_NAME')}} @yield('page_title')</title>
    <link href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css" rel="stylesheet">
</head>
<body class="bg-gray-200 p-4 flex flex-col items-center">
@yield('content')
</body>
</html>
