@extends('layout')
@section('content')
    <div class="container">
        <div class="flex flex-col shadow bg-white border border-gray-300 rounded" style="min-height: 300px">
            <table class="table w-full mb-2">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Phone</th>
                    <th>Operations</th>
                </tr>
                </thead>
                <tbody>
                @foreach($messages as $message)
                    <tr>
                        <td>{{$message->id}}</td>
                        <td>{{$message->name}}</td>
                        <td>{{$message->phone}}</td>
                        <td width="160">
                            <div class="flex flex-row w-full">
                                <a href="{{route('panel.message.delete', ['message' => $message->id])}}"
                                   class="px-4 py-2 mr-2 bg-red-500 hover:bg-red-400 text-white outline-none border border-red-300">
                                    Delete
                                </a>
                                <a href="{{route('panel.message.edit', ['message' => $message->id])}}"
                                   class="px-4 py-2 mr-2 bg-orange-500 hover:bg-orange-400 text-white outline-none border border-orange-300">
                                    Edit
                                </a>
                                <a href="{{route('panel.message.edit',['message' => $message->id, 'readOnly' => '1'])}}"
                                   class="px-4 py-2 mr-2 bg-blue-500 hover:bg-blue-400 text-white outline-none border border-blue-300">
                                    View
                                </a>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{$messages->links()}}
        </div>
    </div>
@endsection
