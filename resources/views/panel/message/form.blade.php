@extends('layout')
@section('content')
    <div class="container">
        <div class="flex flex-col shadow bg-white border border-gray-300 rounded" style="min-height: 300px">
            <form method="post" action="{{route('panel.message.update', ['message' => $message->id])}}"
                  class="flex flex-col w-full p-4">
                {{csrf_field()}}
                <div class="flex flex-row flex-wrap w-full mb-2">
                    <label for="name" class="mr-2" style="flex-basis: 200px">Name</label>
                    <input type="text" class="flex-1 rounded border border-gray-200 p-2" name="name"
                           {{request()->query('readOnly') == '1' ? 'readonly' : ''}}
                           value="{{old('name', $message->name)}}" id="name"/>
                    @error('name')
                    <div
                        class="flex flex-col border rounded border-red-500 bg-red-200 text-red-700 font-bold w-full my-2 p-2">
                        {{$message}}
                    </div>
                    @enderror
                </div>

                <div class="flex flex-row flex-wrap w-full mb-2">
                    <label for="email" class="mr-2" style="flex-basis: 200px">Email</label>
                    <input type="email" class="flex-1 rounded border border-gray-200 p-2" name="email"
                           {{request()->query('readOnly') == '1' ? 'readonly' : ''}}
                           value="{{old('email', $message->email)}}" id="email"/>
                    @error('email')
                    <div
                        class="flex flex-col border rounded border-red-500 bg-red-200 text-red-700 font-bold w-full my-2 p-2">
                        {{$message}}
                    </div>
                    @enderror
                </div>

                <div class="flex flex-row flex-wrap w-full mb-2">
                    <label for="phone" class="mr-2" style="flex-basis: 200px">Phone</label>
                    <input type="tel" class="flex-1 rounded border border-gray-200 p-2" name="phone" id="phone"
                           {{request()->query('readOnly') == '1' ? 'readonly' : ''}}
                           value="{{old('phone', $message->phone)}}"/>
                    @error('phone')
                    <div
                        class="flex flex-col border rounded border-red-500 bg-red-200 text-red-700 font-bold w-full my-2 p-2">
                        {{$message}}
                    </div>
                    @enderror
                </div>

                <div class="flex flex-row flex-wrap w-full mb-2">
                    <label for="message" class="mr-2" style="flex-basis: 200px">Message</label>
                    <textarea id="message" name="message" {{request()->query('readOnly') == '1' ? 'readonly' : ''}}
                    class="flex-1 rounded border border-gray-200 p-2">{{old('message', $message->message)}}</textarea>
                    @error('message')
                    <div
                        class="flex flex-col border rounded border-red-500 bg-red-200 text-red-700 font-bold w-full my-2 p-2">
                        {{$message}}
                    </div>
                    @enderror
                </div>

                @if(request()->query('readOnly') != '1')
                    <div class="flex flex-row w-full mb-2">
                        <button type="submit"
                                class="bg-blue-500 hover:bg-blue-400 text-white px-4 py-2 rounded border border-blue-300">
                            Submit
                        </button>
                    </div>
                @endif
            </form>
        </div>
    </div>
@endsection
