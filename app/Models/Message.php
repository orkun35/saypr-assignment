<?php

namespace App\Models;

use DateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Message
 * @package App\Models
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $message
 * @property DateTime $created_at
 * @property DateTime $updated_at
 */
class Message extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'email', 'phone', 'message'];
}
