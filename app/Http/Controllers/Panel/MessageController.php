<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use App\Http\Requests\Message\MessageSaveRequest;
use App\Models\Message;
use Exception;

class MessageController extends Controller
{
    public function index()
    {
        $messages = Message::query()->paginate(5);

        return view('panel.message.index', compact('messages'));
    }

    public function edit(Message $message)
    {
        return view('panel.message.form', compact('message'));
    }

    public function update(MessageSaveRequest $request, Message $message)
    {
        $message->update($request->validated());

        return redirect()->route('panel.message.edit', ['message' => $message->id, 'readOnly' => '1']);
    }

    public function delete(Message $message)
    {
        try {
            $message->delete();
        } catch (Exception $e) {

        }

        return redirect()->route('panel.message.index');
    }
}
