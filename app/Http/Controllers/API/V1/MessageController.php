<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Message\MessageSaveRequest;
use App\Http\Resources\MessageResource;
use App\Models\Message;
use Exception;

class MessageController extends Controller
{
    public function index()
    {
        $messages = Message::query()->paginate();

        return MessageResource::collection($messages);
    }

    public function show(Message $message)
    {
        return MessageResource::make($message);
    }

    public function store(MessageSaveRequest $request)
    {
        $message = Message::query()->create($request->validated());

        return MessageResource::make($message);
    }

    public function update(MessageSaveRequest $request, Message $message)
    {
        $message->update($request->validated());

        return MessageResource::make($message);
    }

    public function destroy(Message $message)
    {
        $clone = clone $message;
        try {
            $message->delete();
        } catch (Exception $e) {
        }

        return MessageResource::make($clone);
    }
}
