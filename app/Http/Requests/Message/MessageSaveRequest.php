<?php

namespace App\Http\Requests\Message;

use Illuminate\Foundation\Http\FormRequest;

class MessageSaveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:200',
            'phone' => ['required', 'regex:/^905(0[5-7]|[3-5]\d)\d{3}\d{4}$/'],
            'email' => 'required|email|max:200',
            'message' => 'required|max:200',
        ];
    }

    public function attributes()
    {
        return [
            'name' => $this->expectsJson() ? 'name' : 'Name',
            'phone' => $this->expectsJson() ? 'phone' : 'Phone',
            'email' => $this->expectsJson() ? 'email' : 'Email',
            'message' => $this->expectsJson() ? 'message' : 'Message',
        ];
    }

    public function messages()
    {
        return [
            'phone.regex' => 'Phone field must be valid Turkish GSM number',
        ];
    }
}
