<?php

namespace Database\Factories;

use App\Models\Message;
use Illuminate\Database\Eloquent\Factories\Factory;

class MessageFactory extends Factory
{
    protected $model = Message::class;

    public function definition()
    {
        return [
            'name' => $this->faker->unique()->name,
            'email' => $this->faker->unique()->email,
            'phone' => $this->faker->regexify('/^905(0[5-7]|[3-5]\d)\d{3}\d{4}$/'),
            'message' => $this->faker->unique()->sentences(3, true),
        ];
    }
}
