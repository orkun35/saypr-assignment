<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => '/panel', 'namespace' => 'Panel', 'as' => 'panel.'], function () {
    Route::group(['prefix' => '/message', 'as' => 'message.'], function () {
        Route::get('/', 'MessageController@index')->name('index');
        Route::get('/edit/{message}', 'MessageController@edit')->name('edit');
        Route::post('/edit/{message}', 'MessageController@update')->name('update');
        Route::get('/delete/{message}', 'MessageController@delete')->name('delete');
    });
});
